[![Whizlabs Logo](Whizlabs-logo.jpg)](https://www.whizlabs.com)

# [How to use Boto3 with AWS](https://us06web.zoom.us/webinar/register/6916305887745/WN_UpJfOvoASi2iLz4EF_Wilg)

[![Webinar Invitation](Whizlabs-david.jpeg)](https://us06web.zoom.us/webinar/register/6916305887745/WN_UpJfOvoASi2iLz4EF_Wilg)

### Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)

### Code Repository: <https://gitlab.com/soldavid/whizlabs20210905>

![AWS Community Builder](CommunityBuildersLogo.png)

### The Notebook with the code is **whizlabs20210905.ipynb**.

Check out Whizlabs Online Courses & Practice Tests at <https://www.whizlabs.com>

Follow us on Linkedin: <https://www.linkedin.com/company/whizlabs-software/>

Follow us on Twitter: <https://twitter.com/whizlabs>
